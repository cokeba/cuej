var textarea = document.querySelectorAll('textarea');


for(var i = 0; i < textarea.length; i++) {
    textarea[i].style.cssText = 'height:auto; padding:0';
    textarea[i].style.cssText = 'height:' + textarea[i].scrollHeight + 'px';
}
             
var chapeaucode = document.querySelector('#chapeaucode');
var chapeau = document.querySelector('#chapeau');

var selection = document.querySelectorAll('.selection');
var select_unique = document.querySelectorAll('.select_unique');
var close = document.querySelectorAll('.close');


for(var i = 0; i < selection.length; i++) {
    selection[i].addEventListener('click', function () {
        let $id = this.dataset.id;
        document.getElementById($id).style.height = "100vh";
        document.getElementById($id).querySelector('.buttoncoco').addEventListener('click', copy);
    })
    close[i].addEventListener('click', function () {
        let $id = this.dataset.id;
        document.getElementById($id).style.height = "0";
    })
}

function copy() {
    this.parentNode.parentNode.querySelector('.code').select();
    document.execCommand( 'copy' );
}